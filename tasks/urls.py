from django.urls import path
from tasks.views import (
    TaskDetailView,
    TaskListView,
    TaskCreateView,
    TaskUpdateView,
)


urlpatterns = [
    path("mine/", TaskListView.as_view(), name="show_my_tasks"),
    path("<int:pk>/", TaskDetailView.as_view(), name="detail_tasks"),
    path("create/", TaskCreateView.as_view(), name="create_task"),
    path("<int:pk>/complete/", TaskUpdateView.as_view(), name="complete_task"),
]
